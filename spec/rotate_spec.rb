require_relative '../rotate'

describe Matrix do
  it "should return the correct rotated matrix" do
  	expect(Matrix.rotate_matrix(4, 4, 1, [[1, 2, 3, 4],[5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])).to eq([[2, 3, 4, 8], [1, 7, 11, 12], [5, 6, 10, 16], [9, 13, 14, 15]])
  end
end