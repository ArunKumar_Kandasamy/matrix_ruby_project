# # ruby 2.3.0
# # About "rotating matrix"

class Matrix
  def self.rotate_matrix(r, c, n, matrix_arr)
    m = matrix_arr.transpose.transpose
    n.times { rotate_rings(m) }
    m
  end

  private

  def self.rotate_rings(arr)
    nrings = (arr.size/2)
    m = arr.transpose.transpose
    4.times do
      nrings.times { |i| arr[i][i..-i-1] = m[i][i+1..-i-1] << m[i+1][-i-1] }
      rotate_array!(arr)
      rotate_array!(m)
    end
  end

  def self.rotate_array!(arr)
    arr.replace(arr.map!(&:reverse).transpose)
  end 
end


puts "Enter 3 space seperated integer, M N R (M is number of rows, N is numner columns, R is the number of time the matrix has to be rotated)"

matrix_dia = gets.chomp.split

r = matrix_dia[0].to_i
c = matrix_dia[1].to_i
n = matrix_dia[2].to_i

puts "Enter the matrix in line by line with space seperated integer"

matrix_arr = []
r.times do 
  matrix_line = gets.chomp.split
  matrix_arr << matrix_line
end

puts "Output"
Matrix.rotate_matrix(r, c, n, matrix_arr).each do |line|
  puts "#{line.join(" ")}"
end